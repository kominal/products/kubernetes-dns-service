import { info, error, warn } from '@kominal/lib-ts-logging';
import { ENTRYPOINTS } from './environment';
import axios from 'axios';

const availabaleEntrypoints = ENTRYPOINTS.split(',');
export let entrypoints: string[] = [];

export async function checkEntrypointsHealth() {
	try {
		info('Checking entrypoints health...');

		const newEntrypoints: string[] = [];

		for (const entrypoint of ENTRYPOINTS.split(',')) {
			try {
				const addresses = entrypoint.split('|');
				const { status } = await axios.get<any>(`http://${addresses[0]}:8080/ping`);
				if (status === 200) {
					newEntrypoints.push(addresses[0]);
					if (addresses.length > 1) {
						newEntrypoints.push(addresses[1]);
					}
				} else {
					warn(`Health check for entrypoint '${entrypoint}' returned status code ${status}.`);
				}
			} catch (e) {
				warn(`Entrypoint '${entrypoint}' unrachable: ${e}`);
			}
		}

		if (newEntrypoints.length === 0) {
			error(`Could not reach any entrypoint. Falling back to '${availabaleEntrypoints[0]}'.`);
			newEntrypoints.push(availabaleEntrypoints[0]);
		}

		entrypoints = newEntrypoints;
	} catch (e) {
		error('Could not verify loadbalancer health...');
	}
}
